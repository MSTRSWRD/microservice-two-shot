from django.urls import path
from .views import api_shoe_list, api_shoe_details

urlpatterns = [
    path("shoes/", api_shoe_list, name="api_shoe_list"),
    path("shoes/<int:pk>/", api_shoe_details, name="api_shoe_details"),
]