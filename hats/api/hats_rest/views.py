from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name"]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    """
    Collection RESTful API handler for Hats objects in
    the wardrobe.

    GET:
    Returns a dictionary with a single key "hats" which
    is a list of the fabric, style name, color, a picture of the hat, and its id.

    {
        "hats": [
            {
                "id": database id for the hat,
                "fabric": type of fabric,
                "style_name": the number of the wardrobe section,
                "color": the number of the shelf,
                "pic_url": URL to an image of the hat,
            },
            ...
        ]
    }

    POST:
    Creates a hat resource and returns its details.
    {
        "fabric": type of fabric,
        "style_name": the number of the wardrobe section,
        "color": the number of the shelf,
    }
    """
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(id=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_hat_details(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                {"message": "Hat has been deleted successfully"},
                status=200
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message":"Does not exist"}, status=404)
