import React, { useEffect, useState } from 'react';

function AddShoesForm() {

    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureURL] = useState('');
    const [bins, setBins] = useState([]);
    const [bin, setBin] = useState('');


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;

        const binUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const binResponse = await fetch(binUrl, fetchOptions);
        if (binResponse.ok) {
            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureURL('');
            setBin('');
        }
    }

    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value);
    }
    const handleModelNameChange = (e) => {
        const value = e.target.value;
        setModelName(value);
    }
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    }
    const handlePictureURLChange = (e) => {
        const value = e.target.value;
        setPictureURL(value);
    }
    const handleBinChange = (e) => {
        const value = e.target.value;
        setBin(value);
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new shoes<span>&#128095;</span></h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer"
                                id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleModelNameChange} value={model_name} placeholder="Model Name" required type="text" name="model_name"
                                id="model_name" className="form-control" />
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color"
                                id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureURLChange} value={picture_url} placeholder="Picture URL" required type="url" name="picture_url"
                                id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                                <option value="">Where is it located?</option>
                                {bins.map(bin => {
                                    return(
                                        <option key={bin.id} value={bin.id}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default AddShoesForm;
