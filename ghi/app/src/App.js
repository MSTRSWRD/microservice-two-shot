import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatList from "./HatList";
import NewHatForm from "./NewHatForm";
import ShoesList from "./ShoesList";
import NewShoesForm from "./AddShoesForm";

function App() {


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList />} />
            <Route path="new" element={<NewHatForm />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList />} />
            <Route path="new" element={<NewShoesForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
