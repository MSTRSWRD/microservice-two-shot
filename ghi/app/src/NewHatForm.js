import React, { useEffect, useState } from 'react';

function NewHatForm() {

    const [style, setStyle] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [pictureURL, setPictureURL] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.style_name = style;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = pictureURL;
        data.location = location;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const hatResponse = await fetch(hatUrl, fetchOptions);
        if (hatResponse.ok) {
            setStyle('');
            setFabric('');
            setColor('');
            setPictureURL('');
            setLocation('');
        }
    }

    const handleStyleChange = (e) => {
        const value = e.target.value;
        setStyle(value);
    }
    const handleFabricChange = (e) => {
        const value = e.target.value;
        setFabric(value);
    }
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    }
    const handlePictureURLChange = (e) => {
        const value = e.target.value;
        setPictureURL(value);
    }
    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value);
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new hat <span>&#127913;</span></h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleChange} value={style} placeholder="Style name" required type="text" name="style_name"
                                id="style_name" className="form-control" />
                            <label htmlFor="style_name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} value={fabric} placeholder="Fabric type" required type="text" name="fabric"
                                id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric Type</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color"
                                id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureURLChange} value={pictureURL} placeholder="Picture URL" required type="url" name="picture"
                                id="picture" className="form-control" />
                            <label htmlFor="picture">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                                <option value="">Where is it located?</option>
                                {locations.map(location => {
                                    return(
                                        <option key={location.id} value={location.id}>
                                            {location.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default NewHatForm;
