
import { useEffect, useState } from 'react';

function HatList() {

    const [hats, setHats] = useState([]);
    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const deleteHat = async (hatId) => {
        const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, {
            method: 'DELETE',
        });

        if (response.ok) {
            setHats(hats.filter((hat) => hat.id !== hatId));
        } else {
            console.error('Failed to delete hat');
        }
    };



    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Picture Link</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.style_name}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.color}</td>
                            <td>
                                <a href={hat.picture_url} >
                                    <img
                                        src={hat.picture_url} alt="No photo available" width="auto" height="100px" /></a>
                            </td>
                            <td>{hat.location.closet_name}</td>
                            <td><button onClick={() => deleteHat(hat.id)}>Delete Hat</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}



export default HatList;
