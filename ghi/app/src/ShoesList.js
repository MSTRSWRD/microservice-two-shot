import { useEffect, useState } from 'react'


function ShoesList() {

  const [shoes, setShoes] = useState([]);
  const fetchData = async () => {
      const response = await fetch('http://localhost:8080/api/shoes/');
      if (response.ok) {
          const data = await response.json();
          setShoes(data.shoes);
      }
  }

  useEffect(() => {
      fetchData();
  }, []);

  const deleteShoe = async (shoeID) => {
        const response = await fetch(`http://localhost:8080/api/shoes/${shoeID}`, {
            method: 'DELETE',
        });

        if (response.ok) {
            setShoes(shoes.filter((shoe) => shoe.id !== shoeID));
        } else {
            console.error('Failed to delete shoe');
        }
    };

    return (
        <table className="table table-dark table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Closet Name</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={shoe.id}>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.model_name }</td>
              <td>{ shoe.color }</td>
              <td>
                <a href={shoe.picture_url} > <img src={shoe.picture_url} alt="Image not available" width="auto" height ="100px" /> </a>
              </td>
              <td>{ shoe.bin.closet_name }</td>
              <td><button onClick={() => deleteShoe(shoe.id)}>Delete Shoes</button></td>
            </tr>
            );
          })}
        </tbody>
      </table>
    );
}
export default ShoesList;
