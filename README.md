# Wardrobify

Team:

* Austin Quilter - Shoes microservice
* Justin Linder - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

1 - install app into django
    -installed apps
2 - make a simple shoe model
3 - create a view function to show the list of model 
    -check conference-go restful api for help 
4 - configure the view in urls file 
    -include urls in from django 
5 - include the urls in the project url
    -import include and function from django urls
    -add url patterns
6 - test the api using insomnia
    -send a get request to test and verify empty list
7 - implement post functionality
    -update function based view to handle post
    -test post in insomina



## Hats microservice

Build a Hat model that tracks fabric, style, color, a URL for a picture, and it's location in the wardrobe. Will create necessary views and urls that communicate and provide a response. Will create a location value object that polls the location from the wardrobe.
